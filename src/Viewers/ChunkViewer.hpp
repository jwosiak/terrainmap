#ifndef CHUNKVIEWER_HPP
#define CHUNKVIEWER_HPP

#include "../Controllers/ChunkController.hpp"
#include <vector>
#include <glm/glm.hpp>
#include <GL/glew.h>
#include <iostream>



class ChunkViewer
{
  std::vector<ChunkController> controllers;
  std::vector<unsigned int>    indices[9];
  std::vector<unsigned int>    sphereIndices; // Earth's spherical structure's points
  std::vector<glm::vec3>       indexedVertices;
  std::vector<glm::vec3>       sphereVertices;
  GlobalsStructure             gs;
  glm::vec3                    camPos; // @d camera position
  glm::vec3                    scale = glm::vec3(1, 1, 1);
  int                          levelOfDetails = 0;
  int                          viewDimension = 2;
public:
  ChunkViewer (std::vector<std::string> files, std::pair<int,int> latitudeBinds,
    std::pair<int,int> longitudeBinds);
  void AddChunk (ChunkController controller);
  void DrawAll (int LoD);
private:
  void DrawChunk (ChunkController &_controller);
  void DrawSphere (glm::mat4 ProjectionMatrix, glm::mat4 ViewMatrix);
};

#endif // CHUNKVIEWER_HPP
