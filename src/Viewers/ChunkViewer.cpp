#include "ChunkViewer.hpp"
#include "../controls.hpp"
#include <cmath>
#include "../Controllers/shader.hpp"
#include <glm/gtc/matrix_transform.hpp>
#include "../DataLoader.hpp"


using namespace glm;
using namespace std;

ChunkViewer::ChunkViewer(vector<string> files, pair<int, int> latitudeBinds,
  pair<int, int> longitudeBinds)
{
  DataLoader dl;

  GLuint VertexArrayID;
	glGenVertexArrays(1, &VertexArrayID);
	glBindVertexArray(VertexArrayID);

  // cout << "Hello1\n";
  gs.programID = gs.program2DID = LoadShaders( "Shaders/2DShading.vshader",
    "Shaders/Shading2D.fshader" );
  gs.program3DID = LoadShaders( "Shaders/3DShading.vshader",
    "Shaders/Shading3D.fshader" );
  gs.program3DSphereID = LoadShaders( "Shaders/3DShading_Sphere.vshader",
    "Shaders/Shading3D_Sphere.fshader" );

  // cout << "Hello2\n";


  indexedVertices = dl.GenerateGrid();

  GLuint elementbuffer;
	glGenBuffers(1, &elementbuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);

  for (int i = 0; i < 9; i++)
  {
    indices[i] = dl.GridIndices(i + 1);
  }


  vector<vec3> sphereVertices;
  dl.ConstructSphere(60, 60, sphereVertices, sphereIndices,
    indexedVertices.size());

  indexedVertices.insert(
    indexedVertices.end(),
    sphereVertices.begin(),
    sphereVertices.end()
  );

  GLuint sphereElementBuffer;
  glGenBuffers(1, &sphereElementBuffer);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, sphereElementBuffer);
  glNamedBufferData(sphereElementBuffer,
    sphereIndices.size() * sizeof(unsigned int),
    &sphereIndices[0] , GL_STATIC_DRAW);
  gs.sphereElementID = sphereElementBuffer;


  GLuint vertexbuffer;
  glGenBuffers(1, &vertexbuffer);
  glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
  glEnableVertexAttribArray(0);
  glVertexAttribPointer(
    0,                  // attribute
    3,  // size
    GL_FLOAT,           // type
    GL_FALSE,           // normalized?
    0,                  // stride
    (void*)0            // array buffer offset
  );
  glNamedBufferData(vertexbuffer, indexedVertices.size() * sizeof(glm::vec3),
    &indexedVertices[0], GL_STATIC_DRAW);



  gs.elementBufferID = elementbuffer;


  for (string s : files)
  {
    try
    {
      AddChunk(ChunkController(dl.LoadChunk(s, latitudeBinds, longitudeBinds),
        &gs));
      // cout << s << ": loaded successfully!\n";
    }
    catch(string exc)
    {
      cerr << exc;
    }
  }

}


void ChunkViewer::AddChunk(ChunkController _controller)
{
  _controller.InitDataInVBO();
  controllers.push_back(_controller);
}

#include <glfw3.h>

void ChunkViewer::DrawChunk(ChunkController &controller){
  controller.SetUniformsAndVBO(levelOfDetails);

  glDrawElements(
    GL_TRIANGLES,      // mode
    indices[levelOfDetails-1].size(),    // count
    GL_UNSIGNED_INT,   // type
    (void*) (0 * sizeof(unsigned int)) // element array buffer offset
  );
}


void ChunkViewer::DrawSphere(mat4 ProjectionMatrix, mat4 ViewMatrix)
{
  glUseProgram(gs.program3DSphereID);

  mat4 RVP = ProjectionMatrix * ViewMatrix * getSphereRotation();

  glUniform3f(glGetUniformLocation(gs.program3DSphereID, "scale"),
    scale[0], scale[1], scale[2]);

  glUniformMatrix4fv(glGetUniformLocation(gs.program3DSphereID, "RVP"),
    1, GL_FALSE, &RVP[0][0]);


  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gs.sphereElementID);
  // cout << "Hello world3!\n";
  glDrawElements(
    GL_LINES,      // mode
    sphereIndices.size(),    // count
    GL_UNSIGNED_INT,   // type
    (void*) (0 * sizeof(unsigned int)) // element array buffer offset
  );
}


void ChunkViewer::DrawAll(int LoD)
{
  // double start = glfwGetTime();
  mat4 ProjectionMatrix, ViewMatrix;
  bool shouldRedraw = false;

  scale = camZoom();
  if (viewDimension == 2)
  {
    compute2DPosition();
    camPos = camPosition();
  }
  else
  {
    computeMatricesFromInputs();
    ProjectionMatrix = getProjectionMatrix();
    ViewMatrix = getViewMatrix();
  }

  if (switchToAnotherView(viewDimension) != viewDimension)
  {
    if (viewDimension == 2)
    {
      glEnable(GL_DEPTH_TEST);
    	glDepthFunc(GL_LESS);
    }
    else
    {
      glDisable(GL_DEPTH_TEST);
    }
    viewDimension = viewDimension == 2? 3 : 2;
    gs.programID = gs.programID == gs.program2DID?
      gs.program3DID : gs.program2DID;


    // shouldRedraw = true;
  }

  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  if (viewDimension == 3)
  {
    DrawSphere(ProjectionMatrix, ViewMatrix);
  }

  glUseProgram(gs.programID);
  glUniform3f(glGetUniformLocation(gs.programID, "scale"),
    scale[0], scale[1], scale[2]);

  if (viewDimension == 2)
  {
    glUniform3f(glGetUniformLocation(gs.programID, "camPosition"),
      camPos[0], camPos[1], camPos[2]);
  }
  else
  {
    mat4 RVP = ProjectionMatrix * ViewMatrix * getSphereRotation();
    glUniformMatrix4fv(glGetUniformLocation(gs.programID, "RVP"),
      1, GL_FALSE, &RVP[0][0]);
  }


  if (levelOfDetails != LoD)
  {
    levelOfDetails = LoD;
    glNamedBufferData(gs.elementBufferID,
      indices[levelOfDetails-1].size() * sizeof(unsigned int),
      &indices[levelOfDetails-1][0] , GL_STATIC_DRAW);
  }

  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gs.elementBufferID);
  for (int i = 0; i < controllers.size(); i++)
  {
    DrawChunk(controllers[i]);
  }

  // cout << glfwGetTime() - start << endl;
}
