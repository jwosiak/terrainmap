#include "DataLoader.hpp"
#include <fstream>
#include <vector>
#include <cstdlib>
#include <cstdint>

using namespace std;
using namespace glm;


int DataLoader::ParseNum (string data, int &i, bool &wrongFormat)
{
  string numStr;
  while (data[++i] >= '0' && data[i] <= '9')
  {
    numStr += data[i];
  }

  wrongFormat |= numStr == "";
  return atoi(numStr.c_str());
}


pair<int,int> DataLoader::ParseName(string filename)
{
  bool wrongFormat = false;
  int i;
  for (i = filename.length(); i > 0; i--)
  {
    if (filename[i] == '/')
    {
      i++;
      break;
    }
  }

  int latitudeSign;
  switch (filename[i])
  {
  case 'n':
  case 'N':
    latitudeSign = 1;
    break;
  case 's':
  case 'S':
    latitudeSign = -1;
    break;
  default:
    wrongFormat |= true;
  }

  int latitude = latitudeSign * ParseNum(filename, i, wrongFormat);

  int longitudeSign;
  switch (filename[i])
  {
    case 'e':
    case 'E':
    longitudeSign = 1;
    break;
    case 'w':
    case 'W':
    longitudeSign = -1;
    break;
    default:
    wrongFormat |= true;
  }

  int longitude = longitudeSign * ParseNum(filename, i, wrongFormat);

  if (wrongFormat)
  {
    throw string("File name '" + filename + "' has wrong format!\n");
  }

  return make_pair(latitude, longitude);
}


Chunk DataLoader::LoadChunk (string filename, pair<int,int> ignoreLatitude,
  pair<int,int> ignoreLongitude)
{

  pair<int,int> location;
  try
  {
    location = ParseName(filename);
  }
  catch(string exc)
  {
    throw exc;
  }


  if (location.first < ignoreLatitude.first ||
      location.first > ignoreLatitude.second ||
      location.second < ignoreLongitude.first ||
      location.second > ignoreLongitude.second
  ){
    throw string("");
  }


  vector<float> points;
  points.reserve(1201*1201);
  ifstream input (filename, ios::in | ios::binary);

  char *buffer;
  int length = 0;
  if (input.is_open())
  {
    input.seekg(0, input.end);
    length = input.tellg();
    input.seekg(0, input.beg);

    buffer = new char [length];
    input.read(buffer, length);
    input.close();
  }
  else
  {
    throw string("Couldn't open file " + filename + "\n");
  }

  for (int i = 0; i < length; i += 2)
  {
    uint16_t left, right;
    int h;
    left = uint16_t(buffer[i]);
    left &= 0xff;
    right = uint16_t(buffer[i+1]);
    right &= 0xff;
    h = (left << 8) + right;
    if ( ((h >> 15) & 1) == 1)
    {
      h |= 0xffff0000;
    }
    points.push_back(h);
  }

  return Chunk(points, location.first, location.second);
}


vector<vec3> DataLoader::GenerateGrid ()
{
  vector<vec3> grid;
  int n = 1201;

  for (int i = n-1; i >= 0; i--)
  {
    for (int j = n-1; j >= 0; j--)
    {
      grid.push_back(vec3(j, i, 0));
    }
  }
  return grid;
}

vector<unsigned int> DataLoader::GridIndices(int LoD)
{
  int n = 1201;
  vector<unsigned int> indices;

  for (int i = 0; i < n-1; i += LoD)
  {
    for (int j = 0; j < n-1; j += LoD)
    {
      int x = j + LoD < n? j + LoD : 1200;
      int y = i + LoD < n? i + LoD : 1200;

      indices.push_back(x + i * n);
      indices.push_back(j + y * n);
      indices.push_back(j + i * n);

      indices.push_back(j + y * n);
      indices.push_back(x + i * n);
      indices.push_back(x + y * n);

    }
  }
  return indices;
}



void DataLoader::ConstructSphere (
  int HorizontalSegments, int VerticalSegments, vector<vec3> &vertices,
  vector<unsigned int> &indices, int resultIndicesOffset
){
  float dx = 2 * M_PI / HorizontalSegments;
  float dy = 2 * M_PI / VerticalSegments;

  float HorizontalAngle = 0;
  int IndicesOffset = 0;
  for (int i = 0; i < HorizontalSegments; i++)
  {
    float VerticalAngle = 0;
    int PointsCounter = 0;
    for (int j = 0; j < VerticalSegments; j++)
    {
      vertices.push_back(
        vec3(
          cos(HorizontalAngle) * sin(VerticalAngle),
          cos(VerticalAngle),
          sin(HorizontalAngle) * sin(VerticalAngle)
        )
      );
      if (PointsCounter > 1)
      {
        indices.push_back(indices.back());
      }
      indices.push_back(PointsCounter + IndicesOffset);

      PointsCounter++;
      VerticalAngle += dy;
    }
    indices.push_back(indices.back());
    indices.push_back(IndicesOffset);
    IndicesOffset += PointsCounter;
    HorizontalAngle += dx;
  }

  float VerticalAngle = 0;
  for (int i = 0; i < VerticalSegments; i++)
  {
    HorizontalAngle = 0;
    int PointsCounter = 0;
    for (int j = 0; j < HorizontalSegments; j++)
    {
      vertices.push_back(
        vec3(
          cos(HorizontalAngle) * sin(VerticalAngle),
          cos(VerticalAngle),
          sin(HorizontalAngle) * sin(VerticalAngle)
        )
      );
      if (PointsCounter > 1)
      {
        indices.push_back(indices.back());
      }
      indices.push_back(PointsCounter + IndicesOffset);

      HorizontalAngle += dx;
      PointsCounter++;
    }
    indices.push_back(indices.back());
    indices.push_back(IndicesOffset);
    IndicesOffset += PointsCounter;
    VerticalAngle += dy;
  }
  for (int i = 0; i < indices.size(); i++)
  {
    indices[i] += resultIndicesOffset;
  }
}
