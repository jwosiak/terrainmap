#ifndef CHUNK_HPP
#define CHUNK_HPP

#include <vector>

class Chunk{
public:
  std::vector<float>        points;
  const int                 latitude;
  const int                 longitude;
public:
  Chunk (std::vector<float> &_points, int _latitude, int _longitude);
};

#endif // CHUNK_HPP
