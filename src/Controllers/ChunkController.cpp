#include "ChunkController.hpp"
#include <glm/gtc/matrix_transform.hpp>

#include <iostream>

using namespace glm;
using namespace std;

vector<int> ChunkController::pointsBigBuffer;

ChunkController::ChunkController(
  Chunk             _model,
  GlobalsStructure *_gs
)
  : model(_model)
  , gs(_gs)
{

}



void ChunkController::InitDataInVBO ()
{
  GLuint heightsBuffer;
  glGenBuffers(1, &heightsBuffer);
  glBindBuffer(GL_ARRAY_BUFFER, heightsBuffer);
  glNamedBufferData(
    heightsBuffer,
    model.points.size() * sizeof(float),
    &model.points[0], GL_STATIC_DRAW
  );
  placeInBuffer = gs->heightsBufferIDs.size();
  gs->heightsBufferIDs.push_back(heightsBuffer);
}


void ChunkController::SetUniformsAndVBO(int LoD)
{
  glBindBuffer(GL_ARRAY_BUFFER, gs->heightsBufferIDs[placeInBuffer]);
  glEnableVertexAttribArray(1);
  glVertexAttribPointer(
    1,                  // attribute
    1,  // size
    GL_FLOAT,           // type
    GL_FALSE,           // normalized?
    0,                  // stride
    (void*)0            // array buffer offset
  );


  glUniform2f(glGetUniformLocation(gs->programID, "chunkPosition"),
    model.latitude, model.longitude);
}
