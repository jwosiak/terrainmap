#ifndef CHUNKCONTROLLER_HPP
#define CHUNKCONTROLLER_HPP

#include "../Models/Chunk.hpp"
#include <glm/glm.hpp>
#include <GL/glew.h>
#include <vector>

struct GlobalsStructure
{
  GLuint programID;
  GLuint program2DID;
  GLuint program3DID;
  GLuint program3DSphereID;
  GLuint chunkPositionID;
  GLuint elementBufferID;
  GLuint sphereElementID;
  std::vector<GLuint> heightsBufferIDs;
  GLuint vertexBufferID;
};


class ChunkController
{
private:
  GlobalsStructure         *gs;
public:
  Chunk                     model;
  int                       placeInBuffer;
  static std::vector<int>   pointsBigBuffer;

public:
  ChunkController(
    Chunk             _model,
    GlobalsStructure *_gs
  );

  void InitDataInVBO ();
  void SetUniformsAndVBO (int LoD);
};

#endif // CHUNKCONTROLLER_HPP
