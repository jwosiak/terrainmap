#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <iostream>
#include <fstream>

#include <GL/glew.h>

#include <glfw3.h>
GLFWwindow* window;
const int M = 1024 * 1.5, N = 768 * 1.5;

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
using namespace glm;
using namespace std;

#include "common/text2D.hpp"

#include "DataLoader.hpp"
// #include "Controllers/ChunkController.hpp"
#include "Models/Chunk.hpp"
#include "Viewers/ChunkViewer.hpp"
#include "controls.hpp"

void wResize (GLFWwindow* window, int width, int height)
{
  glViewport(0, 0, width, height);
};


int main( int argc, char *argv[] )
{


  vector<string> files;
  pair<int, int> latitudeBinds = {-90,90}, longitudeBinds = {-180, 180};
  if (argc == 2)
  {
    ifstream input(argv[1], ios::in);
    if (input.is_open())
    {
      string directory;
      if (input.good())
      {
        input >> latitudeBinds.first;
        input >> latitudeBinds.second;

        input >> longitudeBinds.first;
        input >> longitudeBinds.second;

        input >> directory;
      }
      while (input.good())
      {
        string s;
        input >> s;
        if (s != "")
        {
          files.push_back(directory + s);
        }
      }
      input.close();
    }
    else
    {
      cerr << "Couldn't find config file!\n";
      return -1;
    }

  }

	if( !glfwInit() )
	{
		fprintf( stderr, "Failed to initialize GLFW\n" );
		getchar();
		return -1;
	}

	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	// Open a window and create its OpenGL context
	window = glfwCreateWindow( M, N, "Terrain", NULL, NULL);
	if( window == NULL ){
		fprintf( stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version of the tutorials.\n" );
		getchar();
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);

	// Initialize GLEW
	glewExperimental = true; // Needed for core profile
	if (glewInit() != GLEW_OK) {
		fprintf(stderr, "Failed to initialize GLEW\n");
		getchar();
		glfwTerminate();
		return -1;
	}

  glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
  glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
  glfwSetWindowSizeCallback(window, wResize);

  glfwPollEvents();
  glfwSetCursorPos(window, M/2, N/2);

	glClearColor(0.0f, 0.0f, 0.3f, 0.0f);


  // glEnable(GL_BLEND);
	// glEnable(GL_CULL_FACE);



  ChunkViewer cv(files, latitudeBinds, longitudeBinds);


	double lastTime = glfwGetTime();
	int nbFrames = 0, lastnbFrames = -1;

  int fpsLimit = 150;
  double timePerFrame = 1.0 / double(fpsLimit);

  int actualLoD;
  initText2D("Fonts/Holstein.DDS");

  char textLOD[256];
  char textFPS[256];
  sprintf(textFPS, "FPS: calculating...");
  char textDIM[10];
  char textPOS[256];
	do{
    CheckLoD();

    if (GetLoD() != 0)
    {
      actualLoD = GetLoD();
      sprintf(textLOD,"Level of details: %d", actualLoD );
    }
    else
    {
      sprintf(textLOD,"Level of details: %d (auto)", actualLoD );
    }


		double currentTime = glfwGetTime();
		nbFrames++;

		if ( currentTime - lastTime >= 1.0 ){ // If last prinf() was more than 1sec ago
      lastnbFrames = nbFrames;
      sprintf(textFPS,"FPS: %d", lastnbFrames );
			// printf and reset
			// printf("%f ms/frame\n", 1000.0/double(nbFrames));
      if (GetLoD() == 0)
      {
        if (nbFrames < 10 && actualLoD < 9)
        {
          actualLoD++;
        }
        if (nbFrames > 100 && actualLoD > 1)
        {
          actualLoD--;
        }
      }


			nbFrames = 0;
			lastTime += 1.0;
		}

    cv.DrawAll(actualLoD);
    auto pos = camPosition();

    sprintf(textDIM, "View: %dD", GetCurrentDimension());

    pos[0]++;
    pos[1]++;
    char vertHemi = pos[1] >= 0? 'N' : 'S';
    char horizHemi = pos[0] >= 0? 'E' : 'W';

    int vertPos = pos[1] >= 0? pos[1] : -pos[1] + 2;
    int horizPos = pos[0] >= 0? pos[0] : -pos[0] + 2;

    sprintf(textPOS, "Position: %c%d, %c%d", vertHemi, vertPos / 2, horizHemi,
      horizPos / 2);
    printText2D(textDIM, 10, 580, 15);
    printText2D(textLOD, 10, 560, 15);
    printText2D(textFPS, 10, 540, 15);
    if (GetCurrentDimension() == 2)
    {
      printText2D(textPOS, 10, 520, 15);
    }
    printText2D("+", 380, 280, 20);

    // glFlush();
    if (glfwGetTime() - currentTime < timePerFrame)
    {
      usleep(1000000 * (timePerFrame - (glfwGetTime() - currentTime)) );
    }
    // cout << glfwGetTime() - currentTime << endl;


    glfwWaitEvents();
		glfwSwapBuffers(window);
	} // Check if the ESC key was pressed or the window was closed
	while( glfwGetKey(window, GLFW_KEY_ESCAPE ) != GLFW_PRESS &&
		   glfwWindowShouldClose(window) == 0 );

  cleanupText2D();

  // glDisableVertexAttribArray(0);
  // glDisableVertexAttribArray(1);
  // glDeleteBuffers(1, &vertexbuffer);
  // glDeleteBuffers(1, &elementbuffer);
  // glDeleteProgram(programID);
  // glDeleteVertexArrays(1, &VertexArrayID);

	// Close OpenGL window and terminate GLFW
	glfwTerminate();

	return 0;
}
