// Include GLFW
#include <glfw3.h>
extern GLFWwindow* window; // The "extern" keyword here is to access the variable "window" declared in tutorialXXX.cpp. This is a hack to keep the tutorials simple. Please avoid this.

// Include GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
using namespace glm;
using namespace std;

#include "controls.hpp"
#include <cmath>
#include <iostream>

glm::mat4 ViewMatrix;
glm::mat4 ProjectionMatrix;
vec3 sphereRotation = vec3( -90.0f, 0.0f, 0.0f);


// Initial position : on +Z
glm::vec3 position = glm::vec3( 0, 1, 0 );
// Initial horizontal angle : toward -Z
float horizontalAngle = 3.14f;
// Initial vertical angle : none
float verticalAngle = -M_PI / 2;
// Initial Field of View
float initialFoV = 45.0f;

float speed = 20.0f;
float mouseSpeed = 0.005f;

vec3 camPosition2D = vec3(0,0,0);
vec3 camZoom2D = vec3(180, 180, 180);
int levelOfDetails = 6;
int dimension = 2;

glm::mat4 getViewMatrix(){
  return ViewMatrix;
}
glm::mat4 getProjectionMatrix(){
  return ProjectionMatrix;
}

mat4 getSphereRotation()
{
  mat4 rotationMatrix;

  for (int i = 0; i < 3; i++)
  {
    rotationMatrix *= glm::rotate(glm::mat4(1.0),
    (glm::mediump_float)sphereRotation[i], glm::vec3(i == 0,i == 1,i == 2));
  }

  return rotationMatrix;
}


int GetCurrentDimension()
{
  return dimension;
}


int switchToAnotherView(int currentDimension)
{
  static double lastSwitch = glfwGetTime();
  double currentTime = glfwGetTime();
  if (currentTime - lastSwitch > 0.3 &&
      glfwGetKey( window, GLFW_KEY_TAB ) == GLFW_PRESS &&
      glfwGetKey( window, GLFW_KEY_TAB ) == GLFW_RELEASE)
  {
    dimension = currentDimension = currentDimension == 2? 3 : 2;
    lastSwitch = currentTime;
  }
  return currentDimension;
}

int GetLoD()
{
  return levelOfDetails;
}

void CheckLoD()
{
  int keyPressed = levelOfDetails;

  for (int i = GLFW_KEY_0; i <= GLFW_KEY_9; i++)
  {
    keyPressed = glfwGetKey( window, i ) == GLFW_PRESS?
      i - GLFW_KEY_0 : keyPressed;
  }

  levelOfDetails = keyPressed;
}

vec3 camPosition()
{
  return camPosition2D;
}

vec3 camZoom()
{
  static double lTime = glfwGetTime();
  double currentTime = glfwGetTime();
  float deltaTime = float(currentTime - lTime);
  if (glfwGetKey( window, GLFW_KEY_Z ) == GLFW_PRESS){
    camZoom2D *= 1 + deltaTime;
    // camPosition2D /= 1 + deltaTime;
  }
  if (glfwGetKey( window, GLFW_KEY_X ) == GLFW_PRESS){
    camZoom2D /= 1 + deltaTime;
    // camPosition2D *= 1 + deltaTime;
  }
  lTime = currentTime;
  return camZoom2D;
}

void compute2DPosition()
{
  static double lTime = glfwGetTime();
  double currentTime = glfwGetTime();
  float deltaTime = float(currentTime - lTime);

  float camSpeed = 2 * camZoom2D[0];

  if (glfwGetKey( window, GLFW_KEY_UP ) == GLFW_PRESS){
    camPosition2D += vec3(0,1,0) * deltaTime * camSpeed;
  }
  if (glfwGetKey( window, GLFW_KEY_DOWN ) == GLFW_PRESS){
    camPosition2D -= vec3(0,1,0) * deltaTime * camSpeed;
  }
  if (glfwGetKey( window, GLFW_KEY_RIGHT ) == GLFW_PRESS){
    camPosition2D += vec3(1,0,0) * deltaTime * camSpeed;
  }
  if (glfwGetKey( window, GLFW_KEY_LEFT ) == GLFW_PRESS){
    camPosition2D -= vec3(1,0,0) * deltaTime * camSpeed;
  }
  lTime = currentTime;
}

void computeMatricesFromInputs(){
	// glfwGetTime is called only once, the first time this function is called
	static double lastTime = glfwGetTime();

	// Compute time difference between current and last frame
	double currentTime = glfwGetTime();
	float deltaTime = float(currentTime - lastTime);

	// Get mouse position
	double xpos, ypos;
	glfwGetCursorPos(window, &xpos, &ypos);

	// Reset mouse position for next frame
	glfwSetCursorPos(window, 1024/2, 768/2);

	// Compute new orientation
	horizontalAngle += mouseSpeed * float(1024/2 - xpos );
	verticalAngle   += mouseSpeed * float( 768/2 - ypos );

  float vertLimit = 0;//- M_PI / 8.;
  verticalAngle = verticalAngle > vertLimit ? vertLimit : verticalAngle;

	// Direction : Spherical coordinates to Cartesian coordinates conversion
	glm::vec3 direction(
		cos(verticalAngle) * sin(horizontalAngle),
    sin(verticalAngle),
    cos(verticalAngle) * cos(horizontalAngle)
	);

	// Right vector
	glm::vec3 right = glm::vec3(
		sin(horizontalAngle - 3.14f/2.0f),
		0,
		cos(horizontalAngle - 3.14f/2.0f)
	);

  float mult = float(pow(camZoom2D[0], 0.4));

	// Up vector
	glm::vec3 up = glm::cross( right, direction );

	if (glfwGetKey( window, GLFW_KEY_UP ) == GLFW_PRESS){
    sphereRotation[0] += deltaTime * speed * mult;
	}
	if (glfwGetKey( window, GLFW_KEY_DOWN ) == GLFW_PRESS){
    sphereRotation[0] -= deltaTime * speed * mult;
	}
	if (glfwGetKey( window, GLFW_KEY_RIGHT ) == GLFW_PRESS){
    sphereRotation[1] -= deltaTime * speed * mult;
	}
	if (glfwGetKey( window, GLFW_KEY_LEFT ) == GLFW_PRESS){
    sphereRotation[1] += deltaTime * speed * mult;
	}
	float FoV = initialFoV;// - 5 * glfwGetMouseWheel(); // Now GLFW 3 requires setting up a callback for this. It's a bit too complicated for this beginner's tutorial, so it's disabled instead.


  position = vec3(0,1.0 / camZoom2D[2] + 0.1 / mult, 0 );

  ProjectionMatrix = glm::perspective(
    FoV, 4.0f / 3.0f,
    float(0.1 / mult / 10),
    position[1] * 100);
  // Camera matrix
	ViewMatrix = glm::lookAt(
    position,           // Camera is here
    position+direction, // and looks here : at the same position, plus "direction"
    up                  // Head is up (set to 0,-1,0 to look upside-down)
 );

	// For the next frame, the "last time" will be "now"
	lastTime = currentTime;
}
