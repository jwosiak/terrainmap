#ifndef CONTROLS_HPP
#define CONTROLS_HPP

int GetCurrentDimension();
int switchToAnotherView(int currentDimension);
int GetLoD();
void CheckLoD();
glm::vec3 camPosition();
glm::vec3 camZoom();
void compute2DPosition();
void computeMatricesFromInputs();
glm::mat4 getViewMatrix();
glm::mat4 getProjectionMatrix();
glm::mat4 getSphereRotation();

#endif
