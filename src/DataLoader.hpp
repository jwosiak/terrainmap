#ifndef DATALOADER_HPP
#define DATALOADER_HPP

#include <iostream>
#include <vector>
#include <glm/glm.hpp>
#include "Models/Chunk.hpp"

class DataLoader
{
public:
  DataLoader () { }
  Chunk LoadChunk (std::string filename, std::pair<int,int> ignoreLatitude,
    std::pair<int,int> ignoreLongitude);

private:
  int ParseNum (std::string data, int &i, bool &wrongFormat);
  std::pair<int,int> ParseName (std::string filename);
public:
  std::vector<glm::vec3> GenerateGrid ();
  std::vector<unsigned int> GridIndices (int LoD);
  void ConstructSphere (
    int HorizontalSegments, int VerticalSegments,
    std::vector<glm::vec3> &vertices,
    std::vector<unsigned int> &indices, int resultIndicesOffset);
};

#endif // DATALOADER_HPP
