Controlls:
  x, y - zoom
  arrows - movement
  tab - switch view dimension
  esc - exit

Program should be executed with config file as parameter.

Format of config file:
<lat1>
<lat2>
<lon1>
<lon2>
<path>
<map_1>
<map_2>
...
<map_n>


where
<lat1> is lower limit of latitude (squares with lower latitude will be ignored)
<lat2> is higher limit of latitude,
<lon1> is lower limit of longitude,
<lon2> is higher limit of longitude

<path> is path of directory, which contains map files (.hgt)
<map_1>...<map_n> are names of map files



Maps can be found at: https://dds.cr.usgs.gov/srtm/version2_1/SRTM3/Eurasia/
