CXX=g++
CFLAGS= -I/home/jakub/anl/include/ -I/home/jakub/anl/include/GLFW/ -std=c++11
LIBS= -lGLEW -lGLU -lglfw3 -lXi -lXrandr -lGL -lXxf86vm -lX11 -lpthread
#COMMON=/home/anl/include/common/shader.o /home/anl/include/common/controls.o /home/anl/include/common/objloader.o /home/anl/include/common/texture.o

VPATH = src src/Models src/Controllers src/Viewers

default: main

.cpp.o:
	${CXX} -c ${CFLAGS} $<

main: main.o DataLoader.o Chunk.o ChunkController.o ChunkViewer.o controls.o text2D.o shader.o texture.o
	${CXX} ${CFLAGS} -L/home/jakub/anl/libs/ DataLoader.o Chunk.o ChunkController.o ChunkViewer.o controls.o shader.o texture.o text2D.o main.o -o prog ${COMMON} ${LIBS}

clean:
	rm -f *.o

distclean:
	rm -f *o prog
