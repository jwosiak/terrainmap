#version 330 core

layout(location = 0) in vec3 vertexPosition_modelspace;
layout(location = 1) in float ht;

uniform vec2 chunkPosition;
uniform vec3 scale;
uniform mat4 RVP;

out vec3 fragmentColor;


void main()
{
    float R = 6367445.0;
    R = ht >= -500? (R + ht) / R : 1;

    float i = vertexPosition_modelspace.y;
    float j = vertexPosition_modelspace.x;

    float HorizontalAngle = 90 + (chunkPosition.y - (j / 1200.0));
    float VerticalAngle = 90 + (chunkPosition.x + (i / 1200.0));

    HorizontalAngle *= (3.14159 * 2.0) / 360.0;
    VerticalAngle *= (3.14159 * 2.0) / 360.0;

    vec3 vPositionWorldspace = vec3(
      -cos(HorizontalAngle) * sin(VerticalAngle) * R,
      -cos(VerticalAngle) * R,
      sin(HorizontalAngle) * sin(VerticalAngle) * R
    );
    // vPositionWorldspace = vec3(HorizontalAngle, VerticalAngle,1);
    // gl_Position = RVP * vec4(vertexPosition_modelspace / scale,1);
    gl_Position = RVP * vec4(vPositionWorldspace / scale,1);


    if (ht <= 0) fragmentColor = vec3(0., 0., 1.0); //blue
    else if (ht < 500)   fragmentColor = vec3(0.,       ht/500,    0.); //->green
    else if (ht < 1000)  fragmentColor = vec3(ht/500-1, 1.,        0.); //->yellow
    else if (ht < 2000)  fragmentColor = vec3(1.,       2.-ht/1000,0.); //->red
    else                 fragmentColor = vec3(1.,1.,1.);                //white
}
