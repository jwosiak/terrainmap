#version 330 core

layout(location = 0) in vec3 vertexPosition_modelspace;

uniform vec3 scale;
uniform mat4 RVP;

void main()
{
  gl_Position = RVP * vec4(vertexPosition_modelspace / scale,1);
}
