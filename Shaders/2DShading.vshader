#version 330 core

layout(location = 0) in vec3 vertexPosition_modelspace;
layout(location = 1) in float ht;

// uniform int pointsBigBuffer[];
// uniform int placeInBuffer;

uniform vec2 chunkPosition;
uniform vec3 camPosition;
uniform vec3 scale;

out vec3 fragmentColor;

void main()
{
  int i = int(vertexPosition_modelspace.y);
  int j = int(vertexPosition_modelspace.x);
  // int ht = pointsBigBuffer[placeInBuffer + j + 1201 * i];
  float dx = 2.0 / 1200.0;
  float dy = 2.0 / 1200.0;
  vec3 localVertPos = vec3(j * -dx + 1, i * dy - 1, 0);
  vec3 vPositionWorldspace = (localVertPos
    + vec3(chunkPosition.y, chunkPosition.x, 0) * 2);


  // vPositionWorldspace /= scale;


  if (ht <= 0) fragmentColor = vec3(0., 0., 1.0); //blue
  else if (ht < 500)   fragmentColor = vec3(0.,       ht/500,    0.); //->green
  else if (ht < 1000)  fragmentColor = vec3(ht/500-1, 1.,        0.); //->yellow
  else if (ht < 2000)  fragmentColor = vec3(1.,       2.-ht/1000,0.); //->red
  else                 fragmentColor = vec3(1.,1.,1.);                //white
  gl_Position = vec4(
    (vPositionWorldspace - camPosition) / scale,
    1
  );
}
